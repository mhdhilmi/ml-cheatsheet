# Data frame manipulation

#### create columns
```
df_daily['duplex'] = 'FDD'
```

#### Change columns name
```
df.columns = ['Name', 'address', 'age']
```

#### Change columns posisiton
```
df.columns = [['address', 'Name', 'age']]
```

#### Delete entire columns
```
del df_daily['duplex_1']
del X_test2['']
```

#### Delete columns
```
df_fdd = df_fdd.drop(['Average_CQI','UE_Rep_Rank_3 (%)','UE_Rep_Rank_4 (%)'], axis=1)
```

#### Delete ALL NA
```
df= df.dropna(how = 'any')
```

### Delete NA from selected columns
```
df.loc[pd.isnull(df['rrc_connected_user'])]
```

### Check NA in spesific columns
```
df.loc[pd.isnull(df['cqi'])]
```

### Get spesific cell string
```
df_daily['type'] = df_daily['EUtranCellFDD'].str[6:7]
```

### Filter all file base on spesific value
```
df_fdd = df.loc[(df['duplex']=='FDD')]
```

### Create new columns and get reference from other cell
```
df['cqi_above_10'] = None
df.loc[df['cqi']>=10, 'cqi_above_10'] = True
df.loc[df['cqi']<10, 'cqi_above_10'] = False
```

### Convert to number
```
df_gcell['eci'] = pd.to_numeric(df_gcell['eci'], errors='coerce').fillna(0)
```

### vlookup
```
dict_longitude = df_gcell.set_index('siteid').to_dict()['longitude']
dict_latitude = df_gcell.set_index('siteid').to_dict()['latitude']
df_gsite['longitude'] = df_gsite['siteid'].map(dict_longitude)
df_gsite['latitude'] = df_gsite['siteid'].map(dict_latitude)
```

### IF logic
```
df_daily['duplex'] = 'FDD'
df_daily['duplex_1'] = df_daily['EUtranCellFDD'].str[7:8]
df_daily.loc[df_daily['duplex_1']=='E','duplex'] = 'TDD'
df_daily.loc[df_daily['duplex_1']=='F','duplex'] = 'TDD'
df_daily.loc[df_daily['duplex_1']=='I','duplex'] = 'MassiveMIMO'
df_daily.loc[df_daily['duplex_1']=='J','duplex'] = 'MassiveMIMO'
```

### Cell operation
```
df['traffic_cs_per_cell'] = df['TRAFFIC_CS']/df['Cell_num']
df['traffic_ps_per_cell'] = df['TRAFFIC_PS']/df['Cell_num']
df['cell_per_km'] = df['Cell_num']/df['area']
```

# File Management
### Read CSV
```
df = pd.read_csv("file_name.csv", delimiter=",", index_col=None, header='infer')
```

### Print variable to csv
```
df_daily.to_csv("file_name.csv", index=False, sep=',')
```

### Combine file
```
list_file_daily_filtered = [
    'data/daily_4g cqi daily kalimantan.csv'
    ,'data/daily_4g cqi daily sumbagteng.csv'
    ,'data/daily_4g cqi daily sumbagut.csv'
]

i = 0
for file in list_file_daily_filtered:
    if i == 0:
        df = pd.read_csv(file, delimiter=",", index_col=None, header='infer')
    else:
        df_new = pd.read_csv(file, delimiter=",", index_col=None, header='infer')
        df = pd.concat([df,df_new], axis=0, ignore_index=True, sort=False)
    i = i + 1
```
# ML Things
### Save to pickle
```
with open('DCR_PSHS.sav', 'wb') as f:
    pickle.dump(RF, f)
```

### Load from pickle
```
loaded_model = pickle.load(open('DCR_PSHS.sav', 'rb'))
pred_dcr_pshs=loaded_model.predict(df_dcr_pshs)
df['pred_DCR_PSHS_ISAT'] = pred_dcr_pshs
```

### Designed correlation
```
import matplotlib.pyplot as plt
import seaborn as sns 

f,ax = plt.subplots(figsize=(25, 25))
sns.heatmap(df.corr(method='spearman'), annot=True, linewidths=.5, fmt= '.1f',ax=ax)
bottom, top = ax.get_ylim()
ax.set_ylim(bottom + 0.5, top - 0.5)
plt.show()

#feature important
feat_imp = pd.DataFrame(
    {'cols':X_train.columns, 
     'imp':RF.feature_importances_
    }).sort_values('imp', ascending=True)

space = np.arange(len(feat_imp.cols))
plt.figure(figsize=(12,8))
plt.barh(space, feat_imp.imp)
plt.yticks(space, feat_imp.cols)
_ = plt.grid(axis='x')
```

### Print rand forest tree (selected only)
```
class_name_cqi = ['cqi_below_10', 'cqi_above_10']

export_graphviz(RF.estimators_[0], out_file='tree.dot', 
                feature_names = X.columns,
                class_names = class_name_cqi,
                rounded = True, proportion = False, 
                precision = 2, filled = True)
cmd = 'dot -Tpng tree.dot -o tree.png -Gdpi=600' %(n,n)
os.system(cmd)
```

### Print rand forest tree (from entire estimator)
```
class_name_cqi = ['cqi_below_10', 'cqi_above_10']
for n in range(RF.n_estimators):
    estimator = RF.estimators_[n]
    export_graphviz(estimator, out_file='tree_%s.dot'%n, 
                    feature_names = X.columns,
                    class_names = class_name_cqi,
                    rounded = True, proportion = False, 
                    precision = 2, filled = True)
    cmd = 'dot -Tpng tree_%s.dot -o tree_%s.png -Gdpi=600' %(n,n)
    os.system(cmd)
```